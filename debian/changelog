nabi (1.0.0-5) UNRELEASED; urgency=medium

  * debian/control: Update to debhelper-compat 13
  * Bump Standards-Version to 4.5.0
  * debian/control: Add 'Rules-Requires-Root: no'
  * debian/rules: Remove -Wl,--as-needed linker flag; the bullseye tool
    chain defaults to it
  * Add Salsa CI config

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 13 May 2020 00:59:18 +0900

nabi (1.0.0-4) unstable; urgency=medium

  * debian/control:
    - Update Vcs-* to the new salsa.d.o repository
    - Add the git branch to Vcs-Git
  * debian/patches/002-cross.patch: Fix FTCBFS. Thanks to Helmut Grohne
  * Standards-Version: 4.3.0
  * Fix misc lintian errors
    - Remove trailing whitespace
    - Upgrade to debhelper 12
    - Use https for the copyright format URL
    - Move theme READMEs to /usr/share/doc/nabi/
  * Update debian/copyright: upstream URLs and years

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 12 May 2019 19:13:12 +0900

nabi (1.0.0-3) unstable; urgency=medium

  * debian/{control,copyright,watch}: Update the upstream URL
  * debian/rules: Enable hardening=+all build
  * debian/copyright: Correct DEP-5 format
  * debian/control: Use https URLs for Vcs-*
  * debian/control: Standards-Version: 3.9.8

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 26 Feb 2017 02:51:53 +0900

nabi (1.0.0-2) unstable; urgency=medium

  [ Changwoo Ryu ]
  * Correct Vcs-* fields
  * Standards-Version: 3.9.5
  * debian/README.Debian.ko: fix typos
  * Stop installing im-switch file
  * debian/docs: Update docs to intall

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 10 May 2014 10:35:15 +0900

nabi (1.0.0-1) unstable; urgency=low

  [ Changwoo Ryu ]
  * New upstream release
  * debian/copyright: Correct format
  * debian/nabi.nbk: Change the license from GFDL to GPL-2+
  * Remove unneeded quilt build dependency
  * Remove deprecated im-switch information on README.Debian
  * Update the upstream homepage
  * Standards-Version: 3.9.4

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 02 Feb 2013 20:41:07 +0900

nabi (0.99.11-2) unstable; urgency=low

  * Suggest imhangul-{gtk2,gtk3} instead of old imhangul
  * debian/watch
    - Rewrite it without using the googlecode redirector.
  * Use debhelper 9
  * Standards-Version: 3.9.3
  * debian/rules:
    - Keep the hardening compiler options while adding -as-needed option.
  * debian/copyright:
    - Use the copyright format 1.0

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 20 May 2012 04:27:11 +0900

nabi (0.99.11-1) unstable; urgency=low

  * New upstream release
  * debian/watch: new upstream download location in Google Code
  * debian/nabi.install: do not install im-config files which are against
    the im-config design

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 31 Dec 2011 20:24:46 +0900

nabi (0.99.10-1) unstable; urgency=low

  * New upstream release
  * Standards-Version: 3.9.2

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 13 Nov 2011 23:10:18 +0900

nabi (0.99.9-1) unstable; urgency=low

  * New upstream release
  * Team maintained by Debian Korean L10N

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 24 May 2011 00:44:22 +0900

nabi (0.99.8-1) unstable; urgency=low

  * New upstream release
  * Build-Depends on the newer libhangul-dev (>= 0.0.12)
  * Simplify shlib dependencies with "-as-needed" link flag

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 24 Jan 2011 02:58:22 +0900

nabi (0.99.7-2) unstable; urgency=low

  * Lower the im-switch priority.
  * Use source format 3.0 (quilt)
  * Standards-Version: 3.9.1
  * Switch to debhelper7 from CDBS
  * im-config support

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 09 Jan 2011 03:43:37 +0900

nabi (0.99.7-1) unstable; urgency=low

  * New upstream release
  * Fix debian/watch for the new KLDP.net site

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 18 Feb 2010 04:02:41 +0900

nabi (0.99.6-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 07 Nov 2009 15:43:29 +0900

nabi (0.99.5-1) unstable; urgency=low

  * New upstream release
  * Build-Depends on the newer libhangul-dev 0.0.10
  * Standard-Version: 3.8.3

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 01 Nov 2009 00:50:49 +0900

nabi (0.99.4-1) unstable; urgency=low

  * New Upstream Version
  * Standard-Version: 3.8.2
  * Fix miscellaneous lintian warnings
  * Use the upstream default icon theme, "Jini" instead of "SimplyRed"

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 05 Jul 2009 15:55:00 +0900

nabi (0.99.3-1) unstable; urgency=low

  * New upstream release
  * Switched to patch system.
  * Specify the BSD like license of symbol.txt.
  * Standard-Version: 3.8.0

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 14 Dec 2008 13:40:46 +0900

nabi (0.99.2-2) unstable; urgency=low

  * Requires libhangul (>= 0.0.8) to build (Closes: #477187)

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 22 Apr 2008 02:24:10 +0900

nabi (0.99.2-1) unstable; urgency=low

  * New upstream release
  * Switched to Docbook XML for man page.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 21 Apr 2008 15:45:00 +0900

nabi (0.99.1-3) unstable; urgency=low

  * Added new Homepage: field to the control file. Removed the homepage
    URL from the description.
  * Cleaned up debian/rules.
  * Added Vcs-Browser and Vcs-Git fields.
  * Updated the manpage.
  * Removed the beginning "A" article from the short description,
    according to Developer Reference 6.2.2.
  * Fixed a lintian error in the old debian/copyright file.

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 28 Feb 2008 06:16:30 +0900

nabi (0.99.1-1) unstable; urgency=low

  * New upstream release
  * Set the default theme as "SimplyRed".

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 06 Jan 2008 06:46:27 +0900

nabi (0.99.0-1) unstable; urgency=low

  * New upstream release
  * Standard-Version: 3.7.3

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 23 Dec 2007 23:49:31 +0900

nabi (0.19-1) unstable; urgency=low

  * New upstream release
  * Removed menu which is not very useful with im-switch.

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 07 Oct 2007 17:18:46 +0900

nabi (0.18-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Thu, 12 Jul 2007 16:15:46 +0900

nabi (0.17-1) unstable; urgency=low

  * New upstream release (Closes: #367047)
  * Switched to CDBS.
  * Added debian/watch file.
  * Wrote more manpage stuff about command line options.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 12 Mar 2007 01:14:00 +0900

nabi (0.15-2) unstable; urgency=low

  * Used im-switch.  Wrote im-switch info in debian/README.Debian and
    debian/README.Debian.ko

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 10 Dec 2004 10:59:21 +0900

nabi (0.15-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Fri, 10 Dec 2004 10:44:10 +0900

nabi (0.14-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 28 Aug 2004 23:02:09 +0900

nabi (0.13-2) unstable; urgency=low

  * Added Build-Depends on libxt-dev (Closes: #251589).  Thanks to Goswin
    von Brederlow.

    RATIONALE: The ./configure script couldn't detect the -lX11 path
    without installing libxt-dev, while Xt is never required for
    compiling or linking.

  * debian/menu: Made the command in the menu an absolute path.

 -- Changwoo Ryu <cwryu@debian.org>  Tue,  1 Jun 2004 11:43:38 +0900

nabi (0.13-1) unstable; urgency=low

  * New upstream release
  * debian/control: Raised the Standard-Version to 3.6.1.
  * debian/menu: Quoted strings in menu items.

 -- Changwoo Ryu <cwryu@debian.org>  Mon, 15 Mar 2004 21:59:52 +0900

nabi (0.12-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 20 Dec 2003 06:41:22 +0900

nabi (0.11-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sun, 30 Nov 2003 01:58:16 +0900

nabi (0.10-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sat,  8 Nov 2003 00:12:39 +0900

nabi (0.9-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Tue, 14 Oct 2003 22:26:22 +0900

nabi (0.8-1) unstable; urgency=low

  * New upstream release

 -- Changwoo Ryu <cwryu@debian.org>  Sat, 11 Oct 2003 23:05:42 +0900

nabi (0.7-1) unstable; urgency=low

  * Initial Release (Closes: #212757).

 -- Changwoo Ryu <cwryu@debian.org>  Wed, 17 Sep 2003 00:40:19 +0900
